import React, { Component } from 'react';
import web3 from "./web3";
import Election from "./Election";
import {
    Button,
    Card,
    CardBody,
    CardHeader,
    Col,
    Form,
    FormGroup,
    Label,
    Row,
} from 'reactstrap';

class Forms extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.toggleFade = this.toggleFade.bind(this);
        this.state = {
            collapse: true,
            fadeIn: true,
            timeout: 300,
            owner: "",
            candidates1: [],
            candidates2: []
        };
    }

    async componentDidMount() {
        const owner = await Election.methods.owner().call();
        const candidates1 = await Election.methods.candidates("0").call();
        const candidates2 = await Election.methods.candidates("1").call();
        const totalVotes = await Election.methods.totalVotes().call();
        this.setState({ owner, candidates1, candidates2, totalVotes });
    }

    onSubmit = async event => {
        event.preventDefault();

        const accounts = await web3.eth.getAccounts();

        this.setState({ message: "Waiting on transaction success..." });

        await Election.methods.vote(this.state.value).send({
            from: accounts[0]
        });

        this.setState({ message: "You chose " });
    };

    toggle() {
        this.setState({ collapse: !this.state.collapse });
    }

    toggleFade() {
        this.setState((prevState) => { return { fadeIn: !prevState } });
    }

    // onClick = async () => {
    //     const accounts = await web3.eth.getAccounts();
    //     const response = await Election.methods.getArray().call();
    //     this.setState({ message: response });
    //   };
    onClick = async () => {
        const response = await Election.methods.getArray().call();
        const responser = response.toString().replace("0x", " 0x");
        this.setState({ message: responser });
    };

    render() {
        return (
            <div className="animated fadeIn">
                <Col xs="12" md="6" style={{ paddingTop: '30px', paddingLeft: '3px' }} >
                    <Card >
                        <CardHeader>
                            <strong>Voter</strong>
                        </CardHeader>
                        <CardBody>
                            <Form action="" method="post" className="form-horizontal">
                                <FormGroup row>
                                    <Col md="10">
                                        <Label htmlFor="hf-email">These are the candidates 0 : <strong>{this.state.candidates1.name}</strong>  {this.state.candidates1.count}{" "} and 1 :  <strong>{this.state.candidates2.name}</strong> !</Label>
                                    </Col>

                                    <Col md="5" style={{ marginTop: '10px' }}>
                                        <Label htmlFor="hf-email">Your Candidate Number</Label>
                                    </Col>
                                    <Col xs="12" md="2" style={{ marginTop: '10px' }}>
                                        <input enable type="text" value={this.state.value}
                                            onChange={event => this.setState({ value: event.target.value })}
                                        />
                                        <Button
                                            type="submit"
                                            size="sm"
                                            color="primary"
                                            style={{ marginTop: '10%' }}
                                            onClick={e => this.onSubmit(e, "tutup")}> Submit</Button>
                                    </Col>
                                </FormGroup>

                                <FormGroup row>
                                    <Col md="3">
                                        <Label htmlFor="hf-email">Total Vote</Label>
                                    </Col>
                                    <Col xs="12" md="9">
                                        <input enable type="text" value={this.state.totalVotes}></input>
                                        {/*<Input type="text" id="hf-email" name="hf-email" autoComplete="email" /> */}
                                        {/* <Button type="submit" size="sm" color="primary" style={{ marginTop: '1%', marginRight: '80%' }}><iclassName="fa fa-dot-circle-o"></i> Submit</Button> */}
                                    </Col>

                                    <Col md="3" style={{ marginTop: '10px' }}>
                                        <Label htmlFor="hf-email">Candidate 1</Label>
                                    </Col>
                                    <Col xs="12" md="9" style={{ marginTop: '10px' }}>
                                        <input enable type="text" value={this.state.candidates1.voteCount}></input>
                                        {/* <Input type="text" id="hf-email" name="hf-email" autoComplete="email" /> */}
                                        {/* <Button type="submit" size="sm" color="primary" style={{ marginTop: '1%', marginRight: '80%' }}><i className="fa fa-dot-circle-o"></i> Submit</Button> */}
                                    </Col>

                                    <Col md="3" style={{ marginTop: '15px' }}>
                                        <Label htmlFor="hf-email">Candidate 2</Label>
                                    </Col>
                                    <Col xs="12" md="9" style={{ marginTop: '15px' }}>
                                        <input enable type="text" value={this.state.candidates2.voteCount}></input>
                                    </Col>
                                    <Col xs="12" md="9" style={{marginTop:'15px'}}>
                                        <input enable type="text" value={this.state.candidates2.voteCount}></input>
                                    </Col>                                   
                                </FormGroup>
                            </Form>
                        </CardBody>
                        {/* <CardFooter>
                            <Button type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                            <Button type="reset" size="sm" color="danger" style={{ marginLeft: '10px' }}><i className="fa fa-ban"></i> Reset</Button>
                        </CardFooter> */}
                    </Card>
                </Col>

            </div>
        );
    }
}

export default Forms;