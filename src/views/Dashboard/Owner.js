import React, { Component } from 'react';
import {
    Badge,
    Button,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Fade,
    Form,
    FormGroup,
    FormText,
    FormFeedback,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupButtonDropdown,
    InputGroupText,
    Label,
    Row,
} from 'reactstrap';

class Owners extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.toggleFade = this.toggleFade.bind(this);
        this.state = {
            collapse: true,
            fadeIn: true,
            timeout: 300
        };
    }

    toggle() {
        this.setState({ collapse: !this.state.collapse });
    }

    toggleFade() {
        this.setState((prevState) => { return { fadeIn: !prevState } });
    }

    render() {
        return (
            <div className="animated fadeIn">
                <Col xs="12" md="6" style={{ paddingTop: '30px', paddingLeft: '3px' }}>
                    <Card >
                        <CardHeader>
                            <strong>Open Campaigns</strong>
                        </CardHeader>
                        <CardBody>
                            <Form action="" method="post" className="form-horizontal">
                                <FormGroup row>
                                    <Col md="3">
                                        <Label htmlFor="hf-email">Hashcode</Label>
                                    </Col>
                                    <Col xs="12" md="9">
                                        <Input type="select" id="hf-email" name="hf-email" placeholder="Enter Email..." >
                                        <option value="">--Please choose an option--</option>
                                            <option>0x38a1aD6ABFC434f50e559a296F42792B8A13F063</option>
                                            <option >0x38a1aD6ABFC434f50e559a296F42792B8A13F063</option>
                                            <option >0x1708Be7e8aE42FbcF26D4DAC94d1561d099E6dFC</option>
                                            <option >0xb0750D7C9C5E20586C7B88bb2eA79Cb49B030351</option>
                                            <option >0x1708Be7e8aE42FbcF26D4DAC94d1561d099E6dFC</option>
                                            <option >0xb0750D7C9C5E20586C7B88bb2eA79Cb49B030351</option>
                                        </Input>
                                        {/* <FormText className="help-block">Choose ...</FormText> */}
                                    </Col>
                                </FormGroup>
                            </Form>
                        </CardBody>
                        <CardFooter>
                            <Button type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                            <Button type="reset" size="sm" color="danger" style={{ marginLeft: '10px' }}><i className="fa fa-ban"></i> Reset</Button>
                        </CardFooter>
                    </Card>
                </Col>

            </div>
        );
    }
}

export default Owners;